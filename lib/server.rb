require "./lib/init"

disable :logging
set :root, File.dirname(__FILE__) + "/../"

get "/" do
  File.readlines("public/index.html")
end

get "/musiclibrary" do
  content_type "application/json"
  File.readlines("public/musiclibrary.json")
end

get "/favicon.ico" do
  ""
end

