
// NOTE FOR RUNNING TESTS

// line no. 133 in MusicPlayer.js should be commented for all the tests to pass
// othervise there is an error with EVERYTHING

var musicLibraryData = [{
    tracks : [
        {
            "artist": "Artist A",
            "title": "Track A",
            "url": "/music/Artist A Track A.mp3"
        },
        {
            "artist": "Artist B",
            "title": "Track B",
            "url": "/music/Artist B Track B.mp3"
        }
    ]
}];

var fakeServer;    // this is faking a server so we can call > library.fetch() in $("document").ready
var fixtures;

beforeEach(function() {
  fixtures = {
    musiclibrary : {
      valid:[{ 
              "artist": "Artist A",
              "title": "Track A",
              "url": "/music/Artist A Track A.mp3"
            },
            {
              "artist": "Artist B",
              "title": "Track B",
              "url": "/music/Artist B Track B.mp3"
            }]
      , error500 : {
        }
    }
  }
  
  
  fakeServer = sinon.fakeServer.create();
  fakeServer.respondWith(   "GET"
                          , "/musiclibrary"
                          , [ 
                                 200
                               , { "Content-Type": "application/json" }
                               , JSON.stringify(fixtures.musiclibrary.valid)
                            ]
                        );
});

afterEach(function() {   // server has to be reset after each test
  fakeServer.restore();
})


describe("Track model", function() {
  var track;
  
  //describe("exposing attributes", function() {
    beforeEach(function() {
      track = new MusicPlayer.Track(musicLibraryData[0].tracks[0]);
    
      // pre-conditions check
      expect(track).toBeDefined();
    });
  
    afterEach(function() {
    
    });
  
    it("should expose an attribute artist", function() {
      expect(track.get("artist")).toEqual("Artist A");
    });
  
    it("should expose an attribute title", function() {
      expect(track.get("title")).toEqual("Track A");
    });
  
    it("should expose an attribute URL", function() {
      expect(track.get("url")).toEqual("/music/Artist A Track A.mp3");
    });
  //});
  
  describe("url", function() {
    beforeEach(function() {
      var collection = {     // we are stubbing a collection here
        url: "/collection"
      };
      track.collection = collection;   // we are stubbing a collection for the model here
    });
    
    describe("when no id is set", function(){
      it("should return the collection URL", function(){   // here we are checking that a model takes over a collection's url
        expect(track.url()).toEqual("/collection");
      });
    });
    
    describe("when id is set", function(){
      it("should return the collection URL and id", function(){  // since we do not have ID's this test may not be necessary, but still it's okay
        track.id = 1;
        expect(track.url()).toEqual("/collection/1");
      })
    });
    
  });
  
});


describe("MusicPlayer.Library collection", function () {
  var musicLibrary
    , stubModelTrack
    , modelTrack;
    
  describe("adding a model to collection", function() {
    beforeEach(function () {
      stubModelTrack = sinon.stub(MusicPlayer, "Track");
      modelTrack = new Backbone.Model({
        "artist": "Artist A",
        "title": "Track A",
        "url": "/music/Artist A Track A.mp3"
      });
    
      stubModelTrack.returns(modelTrack);  //  Track stub also returns modelTrack
    
      musicLibrary = new MusicPlayer.Library();
      musicLibrary.model = MusicPlayer.Track;  // this is a function that returns our modelTrack object
    
      //console.log(musicLibrary.at(0).get("artist"));
      //var myTrack = new MusicPlayer.Track({ id : 1, "artist" : "Someone"})

      musicLibrary.add({});  // we can pass any object here. The model in the collection will always be 'modelTrack'

      //console.log(musicLibrary.at(0).get("artist"));

      // console.log(musicLibrary.at(0) instanceof Backbone.Model);        // true
      // console.log(musicLibrary.at(0) instanceof MusicPlayer.Track);     // false, model track is not an instance of MusicPlayer.Track stub. It is just returned by that funcion.
      // console.log(typeof musicLibrary.at(0));                           // true - this is a Backbone.Model, so it is an object
    
      // pre-conditions
      expect(musicLibrary).toBeDefined();
      expect(musicLibrary.length).toBeDefined();
    });

    afterEach(function() {
      stubModelTrack.restore();
    });

    it("gets created from data", function () {
      expect(musicLibrary.length).toEqual(1);
      expect(musicLibrary.length).toNotEqual(2);
    });
  
    it("should return appropriate model", function() {
      //console.log(musicLibrary.at(0).get("artist"));
      //console.log(stubModelTrack().get("artist"));    // the trick here is not to forget to instantiate stubModelTrack function!!!
      expect( musicLibrary.at(0).get("artist")).toBe(stubModelTrack().get("artist"));
    });
  });
  
  describe("server request", function() {
    beforeEach(function() {
      // we are using a fake server response here -- it is defined on the top of the document
      musicLibrary = new MusicPlayer.Library();
    });

    
    it("should make the correct request", function() {
      musicLibrary.fetch();
      expect(fakeServer.requests.length).toEqual(1);
      expect(fakeServer.requests[0].method).toEqual("GET");
      expect(fakeServer.requests[0].url).toEqual("/musiclibrary");
    });
    
    it("should parse tracks from the response", function() {
      musicLibrary.fetch();
      fakeServer.respond();
      // console.log(musicLibrary.at(0).get('artist'));
      // console.log(fixtures.musiclibrary.valid.response.tracks.length);
      expect(musicLibrary.length).toEqual(fixtures.musiclibrary.valid.length);
      expect(musicLibrary.at(0).get('artist')).toEqual(fixtures.musiclibrary.valid[0].artist)
    })
  });
  
});

describe("MusicPlayer.LibraryView", function() {
  beforeEach(function() {
    var libraryView;
    libraryView = new MusicPlayer.LibraryView();
  });
  
  describe("Instantiation of template", function() {
    it("should create a template root element", function() {
      expect($(libraryView.el).find('h3')).toHaveText("All tracks in the library");
    });
    
    it("should have a class 'tracks'", function() {
      expect($(libraryView.el).find('ul')).toHaveClass("tracks");
    })
  });
  
  
  describe("rendering", function() {
    beforeEach(function() {
      //var trackView, trackViewStub, trackRenderSpy, trackViewStub, track1, track2, track3;

      this.trackView = new Backbone.View();
      this.trackView.render = function(){
        this.el = document.createElement('li');
        return this;
      };
      this.trackRenderSpy = sinon.spy(this.trackView, "render");
      this.trackViewStub = sinon.stub(MusicPlayer, "TrackView").returns(this.trackView);
      this.track1 = new Backbone.Model({"artist": "Artist A", "title": "Track A", "url": "/music/Artist A Track A.mp3"});
      this.track2 = new Backbone.Model({"artist": "Artist B", "title": "Track B", "url": "/music/Artist B Track B.mp3"});
      this.track3 = new Backbone.Model({"artist": "Artist C", "title": "Track C", "url": "/music/Artist C Track C.mp3"});
      
      libraryView.collection = new Backbone.Collection([
        this.track1,
        this.track2,
        this.track3
      ]);
      console.log(libraryView.render());  //  3 x <li></li> are generated - this can be seen in the console. This looks ok

      //libraryView.render();
    });
    
    afterEach(function(){
      MusicPlayer.TrackView.restore();
    });
    
    it("returns the view object", function() {
      expect(libraryView.render()).toEqual(libraryView);
    });
    
    it("should create a Track view for each track item", function() {
      //console.log(this.trackViewStub()); // sinon properties called, calledThrice can be seen here
      expect(this.trackViewStub).toHaveBeenCalledThrice();      // jasmine-sinon matcher enables to have been called thrice
      //expect(this.trackViewStub.calledThrice).toBeTruthy();   // this does the same as the previous test
      //expect(this.trackViewStub.called).toBeTruthy();         // this works too
    });
  
    it("should have been called with each model", function() {
      expect(this.trackViewStub).toHaveBeenCalledWith({model: this.track1, collection: libraryView.collection});
      expect(this.trackViewStub).toHaveBeenCalledWith({model: this.track2, collection: libraryView.collection});
      expect(this.trackViewStub).toHaveBeenCalledWith({model: this.track3, collection: libraryView.collection});      
    });
    
    it("should render each Track view", function() {
      expect(this.trackView.render).toHaveBeenCalledThrice();
    });
    
    it("appends the track to the Library list", function() {

      expect($(libraryView.el).find("ul.tracks").children().length).toEqual(3);
    });
    
  });
  
});


describe("MusicPlayer.TrackView", function() {
  beforeEach(function() {
    this.model = new Backbone.Model({
      "artist": "Artist A", "title": "Track A", "url": "/music/Artist A Track A.mp3"
    });
    this.view = new MusicPlayer.TrackView({model: this.model});
    setFixtures('<ul class="tracks"></ul>');
  });
  
  describe("rendering", function() {
    it("returns the view object - tests return this;", function() {
      expect(this.view.render()).toEqual(this.view);
    });
  });
  
  describe("template", function() {
    beforeEach(function() {
      this.view.render();
    });
    
    it("has the correct images", function() {
      //console.log($(this.view.el).find('button img').eq(1));
      expect($(this.view.el).find('button img').eq(0)).toHaveAttr('src', '/images/add.png');
      expect($(this.view.el).find('button img').eq(1)).toHaveAttr('src', '/images/remove.png');
    });
    
    it("has the correct artist", function() {
      expect($(this.view.el).find('span').eq(0)).toHaveText('Artist A');
    });
    
    it("has the correct title name", function() {
      expect($(this.view.el).find('span').eq(1)).toHaveText('Track A');
    });
    
  });
  
  describe("template on fixture", function() {
    beforeEach(function() {
      $('.tracks').append(this.view.render().el);
    });
    
    it("has the correct images", function() {
      expect($(".tracks").find('button img').eq(0)).toHaveAttr('src', '/images/add.png');
      expect($(".tracks").find('button img').eq(1)).toHaveAttr('src', '/images/remove.png');
    });
    
    it("has the correct artist", function() {
      expect($(".tracks").find('span').eq(0)).toHaveText('Artist A');
    });
    
    it("has the correct title name", function() {
      expect($(".tracks").find('span').eq(1)).toHaveText('Track A');
    });
    
    
    describe("when add button handler is clicked", function() {
      beforeEach(function(){
//console.log("this.view instanceof MusicPlayer.TrackView: " + (this.view instanceof MusicPlayer.TrackView))
        this.spyClickSelect = sinon.spy(this.view, "select");

        this.librarySpy = sinon.spy();
        this.collection = new Backbone.Collection({ });
        this.collection.bind('select', this.librarySpy);
        this.view.collection = this.collection;

        this.li = $('.tracks li:first');

      });
      
      afterEach(function(){        // spies don't need restoring?
        // this.librarySpy.restore(); // ne treba restore jer nije na model/collection/view bindan nego kao event callback => ne mijenja model/col/view
        this.spyClickSelect.restore();
      })
      
      // ovaj test spaja sljedeca 2 u jedan, zgodnije je imati odvojeno da lakse nadjes mjesto greske
      // 1) nije bindan click event na handler funkciju => u viewu
      // 2) ili collection ne trigerrira custom 'select' event => u collectionu
      it("should trigger a select event on collection", function() {
        expect(this.librarySpy).not.toHaveBeenCalled();
        this.li.find('button.add').trigger('click');     // we are triggering a click on add button
        expect(this.librarySpy).toHaveBeenCalled();
      });

      // ovo je bolja varijanta, ali prvi test faila => zato je disablean (xit) => treba skuzit sto ga muci sa spyjem
      xit("should bind click event to 'select' method", function() {
        this.li.find('button.add').trigger('click');     // we are triggering a click on add button
        expect(this.spyClickSelect).toHaveBeenCalledOnce();
      });

      it("should have select event on collection triggered when 'select' method invoked", function() {
        expect(this.librarySpy).not.toHaveBeenCalled();
        this.view.select();
        expect(this.librarySpy).toHaveBeenCalledOnce();
      })
    });
    
    describe("when remove button handler is clicked", function() {
      beforeEach(function(){
        this.collection = new Backbone.Collection({ });
        this.view.options.playlist = this.collection;
        this.libraryRemoveStub = sinon.stub(this.collection, "remove");
      
        this.li = $('.tracks li:first');
        this.li.find('button.remove').trigger('click');     // we are triggering a click on add button
      
      });
      
      it("should call a remove method on options.playlist", function() {
        expect(this.libraryRemoveStub).toHaveBeenCalled();
      });
    });
    
  });
});


describe("MusicPlayer.Playlist collection", function() {
  beforeEach(function(){
    this.playlist = new MusicPlayer.Playlist();
    this.model = new Backbone.Model({
      "artist": "Artist A",
      "title": "Track A",
      "url": "/music/Artist A Track A.mp3"
    });
  });
  
  
  describe("adding a model to collection", function() {
    beforeEach(function(){
      this.trackStub = sinon.stub(MusicPlayer, "Track");
      this.trackStub.returns(this.model);
      this.playlist.model = MusicPlayer.Track;
      this.playlist.add({});   // we can add an empty object since we are testing just colling of add method
    });
    
    afterEach(function(){
      this.trackStub.restore();
    });
    
    it("should add a model", function(){
      expect(this.playlist.length).toEqual(1);
    });
    
    it("should find get the attribute with right value", function(){
      expect(this.playlist.at(0).get('artist')).toEqual("Artist A");
    });
    
  });
  
  describe("custom methods", function() {
    beforeEach(function(){
      this.playlist.add(this.model);
    });
    
    it("isFirstTrack should return true if it is the first track", function(){
      expect(this.playlist.isFirstTrack(0)).toBeTruthy();
    });
    
    it("isFirstTrack should return false if it is not the first track", function(){
      expect(this.playlist.isFirstTrack(2)).toBeFalsy();      
    });
    
    it("isLastTrack should return true for argument when it is the last track", function() {
      expect(this.playlist.isLastTrack(0)).toBeTruthy();
    });
    
    it("isLastTrack should return false for argument that is not the index of last track", function(){
      expect(this.playlist.isLastTrack(2)).toBeFalsy();
    });
    
    it("lastTrack should return the index of last model in collection", function(){
      expect(this.playlist.lastTrack()).toEqual(this.playlist.length - 1 );
    });
    
    it("trackUrlAtIndex should return the correct url of model", function(){
      expect(this.playlist.trackUrlAtIndex(0)).toEqual(this.model.get('url'));
    });
    
  });
  
});


describe("MusicPlayer.Player model", function(){
  beforeEach(function(){
    this.player = new MusicPlayer.Player();
  });
  
  describe("when instantiated", function(){
    it('should have currentTrackIndex at zero', function(){
      expect(this.player.get('currentTrackIndex')).toEqual(0);
    });
    
    it('should have stopped state', function(){
      expect(this.player.get('state')).toEqual('stop');
    })
  });
  
  describe("custom methods", function(){
    it('should set the state to play when played', function(){
      this.player.play();
      expect(this.player.get('state')).toEqual('play');
    });
    
    it('should set the state to pause when paused', function(){
      this.player.pause();
      expect(this.player.get('state')).toEqual('pause');
    });
    
    it('isPlaying should return true when state is "play"', function(){
      this.player.play();
      expect(this.player.isPlaying()).toBeTruthy();
    });
    
    it('isPlaying should not return true when state is not play', function(){
      this.player.play();
      this.player.pause();
      expect(this.player.isPlaying()).toBeFalsy();
    });
    
    it('isStopped should return true when state is "paused" or "stop"',function(){
      // this.player.pause();
      expect(this.player.isStopped()).toBeTruthy();
      this.player.play();
      this.player.pause();
      expect(this.player.isStopped()).toBeTruthy();
    });
    
    it('isStopped should return true when state is "playing"', function(){
      this.player.play();
      expect(this.player.isStopped()).toBeFalsy();
    });
    
  });
  
  describe("custom methods with playlist", function(){
    beforeEach(function(){
      this.playlistStubAt = sinon.stub(playlist, "at");     // playlist is defined as a global object in an app - it can be seen here too
      this.playlistStubTrackUrlAtIndex = sinon.stub(playlist, "trackUrlAtIndex");
      
      playlist.add([{ "artist": "Artist A", "title": "Track A", "url": "/music/Artist A Track A.mp3" },
                    { "artist": "Artist B", "title": "Track B", "url": "/music/Artist B Track B.mp3" }]);
    });
    
    afterEach(function(){
      this.playlistStubAt.restore();
      this.playlistStubTrackUrlAtIndex.restore();
      playlist._reset();
    });
    
    it('currentTrack should call playlist at method with appropriate parameter', function(){
      this.player.currentTrack();
      expect(this.playlistStubAt).toHaveBeenCalled();
      expect(this.playlistStubAt).toHaveBeenCalledWithExactly(this.player.get('currentTrackIndex'));
    });
    
    it('currentTrackUrl should call playlist trackUrlAtIndex method with appropriate parameter', function(){
      this.player.currentTrackUrl();
      expect(this.playlistStubTrackUrlAtIndex).toHaveBeenCalled();
      expect(this.playlistStubTrackUrlAtIndex).toHaveBeenCalledWithExactly(this.player.get('currentTrackIndex'));
    });
    
    it('nextTrack should set "currentTrackIndex" to the next track', function(){
      expect(this.player.get('currentTrackIndex')).toEqual(0);
      this.player.nextTrack();
      expect(this.player.get('currentTrackIndex')).toEqual(1);
      expect(this.player.get('currentTrackIndex')).toNotEqual(0);

    });
    
    it('nextTrack should set "currentTrackIndex" to the first track if current track is last', function(){
      this.player.nextTrack();
      this.player.nextTrack();
      expect(this.player.get('currentTrackIndex')).toEqual(0);
      expect(this.player.get('currentTrackIndex')).toNotEqual(1);
    });
    
    it('prevTrack should set "currentTrackIndex" to the previous track or to the last track (if it was first track)', function(){
      this.player.nextTrack(); // current track is now at 1
      expect(this.player.get('currentTrackIndex')).toEqual(1);
      this.player.prevTrack();
      expect(this.player.get('currentTrackIndex')).toEqual(0);
      this.player.prevTrack();
      expect(this.player.get('currentTrackIndex')).toEqual(1);
      expect(this.player.get('currentTrackIndex')).toNotEqual(0);      
    });
    
  });
  
});


describe("PlaylistView ", function(){
  
  beforeEach(function(){
    this.playlist = new MusicPlayer.Playlist();
    this.playlistStub = sinon.stub(this.playlist);
    
    this.library = new MusicPlayer.Library();
    this.libraryStub = sinon.stub(this.library);
    
    this.player = new MusicPlayer.Player();
    this.playerStub = sinon.stub(this.player);
    
    this.playlistView = new MusicPlayer.PlaylistView({collection: this.playlist, library: this.library, player: this.player});
    this.element = this.playlistView.render();
  });
  
  describe("Initialization", function(){
    it("should bind 3 methods to playlist collection events", function(){
      expect(this.playlistStub.bind).toHaveBeenCalledThrice();
      expect(this.playlistStub.bind).toHaveBeenCalledWith('reset', this.playlistView.render);
      expect(this.playlistStub.bind).toHaveBeenCalledWith('add', this.playlistView.renderTrack);
      expect(this.playlistStub.bind).toHaveBeenCalledWith('remove', this.playlistView.renderWithObjects);
    });
    
    it("should bind one method to library collection event", function(){
      expect(this.libraryStub.bind).toHaveBeenCalledOnce();
      expect(this.libraryStub.bind.withArgs('tralala', this.queueTrack)).toBeTruthy();
    });
    
    it("should bind 2 methods to the player model events", function(){
      expect(this.playerStub.bind).toHaveBeenCalledTwice();
      expect(this.playerStub.bind).toHaveBeenCalledWith('change:currentTrackIndex', this.playlistView.updateTrack);
      expect(this.playerStub.bind).toHaveBeenCalledWith('change:state', this.playlistView.updateTrack);
    });
    
  });
  
  describe("Render", function(){
    it("should return itself", function(){
      expect(this.element).toEqual(this.playlistView);
    });
    
    it("should have ul element with 'tracks' class", function(){
      expect($(this.element.el).find('ul')).toExist();
      expect($(this.element.el).find('ul')).toHaveClass('tracks');
    });
  });
  
  describe("custom methods", function(){
    describe("renderTrack method", function(){
      beforeEach(function(){
        this.trackView = new Backbone.View();
        this.trackViewStub = sinon.stub(MusicPlayer, "TrackView").returns(this.trackView);
        this.trackView.render = function(){
          this.el = document.createElement('li');
          return this;
        };
        this.trackViewRenderSpy = sinon.spy(this.trackView, "render");
      });
      
      afterEach(function(){
        MusicPlayer.TrackView.restore();
        this.trackViewRenderSpy.restore();
      });
      
      it("should call apropriate stubs and spies", function(){
        this.playlistView.renderTrack({});
        expect(this.trackViewStub).toHaveBeenCalledOnce();
        expect(this.trackViewRenderSpy).toHaveBeenCalledOnce();
      });
      
      xit("should have new li element - no it should fail with this", function(){
        console.log($(this.element.el).find('ul'), this.$('ul'));
        expect($(this.element.el).find('ul').children().length).toEqual(1);
      });
            
    });
    
    describe("queueTrack method", function(){
      it("should add track to collection", function(){
        this.playlistView.queueTrack({});
        expect(this.playlistStub.add).toHaveBeenCalledOnce();
        expect(this.playlistStub.add).toHaveBeenCalledWith({});        
      });
    });
    
    describe("updateTrack method", function(){
      it("should get player currentTrackIndex", function(){
        this.playlistView.updateTrack();
        expect(this.playerStub.get).toHaveBeenCalled();
      });
    });
    
    describe("renderWithObjects method", function(){
      beforeEach(function(){
        this.trackView = new Backbone.View();
        this.trackView.render = function() {
          this.el = document.createElement('li');
          return this;
        };
        this.trackViewRenderSpy = sinon.spy(this.trackView, "render");
        
        this.trackViewStub = sinon.stub(MusicPlayer, "TrackView").returns(this.trackView);
        this.track1 = new Backbone.Model({id:1});
        this.track2 = new Backbone.Model({id:2});
        this.track3 = new Backbone.Model({id:3});
        
        this.playlistView.collection = new Backbone.Collection([
          this.track1, this.track2, this.track3
        ]);

        this.playlistView.renderWithObjects();
      });
      
      afterEach(function(){
        MusicPlayer.TrackView.restore();
      });

      it("should create a Track view for each todo item", function(){
        expect(this.trackViewStub).toHaveBeenCalledThrice();
        expect(this.trackViewStub).toHaveBeenCalledWith({model: this.track1, playlist: this.playlistView.collection });
        expect(this.trackViewStub).toHaveBeenCalledWith({model: this.track2, playlist: this.playlistView.collection });
        expect(this.trackViewStub).toHaveBeenCalledWith({model: this.track3, playlist: this.playlistView.collection });                
      });
      
      it("should render each Track view twice", function(){
        expect(this.trackView.render).toHaveBeenCalledThrice();
      });
      
      it("appends the tracks to the playlist view", function(){
        expect($(this.playlistView.el).find('.tracks').children().length).toEqual(3);
      });
    });
    
    
  });
  
});









describe("PlayerView", function(){

  describe("Initialization", function(){  // this check has to be before all others because of order of stubs
    beforeEach(function(){
      this.player = new MusicPlayer.Player();
      this.playerStub = sinon.stub(this.player);
      this.view = new MusicPlayer.PlayerView({player: this.player});
    });
  
    it("should have bound to player events", function(){
      expect(this.player.bind).toHaveBeenCalledTwice();
      expect(this.player.bind).toHaveBeenCalledWith('change:state', this.view.updateState);
      expect(this.player.bind).toHaveBeenCalledWith('change:currentTrackIndex', this.view.updateTrack);
    });
    
  });


  beforeEach(function(){
    this.player = new MusicPlayer.Player();
    this.view = new MusicPlayer.PlayerView({player: this.player});
    setFixtures('<div class="player"></div>');
    $('.player').append(this.view.render().el);
  });  
  
  describe("Render", function(){
    
    it("returns the view object", function(){
      expect(this.view.render()).toEqual(this.view);
    });
    
    it("should have a nav element", function(){
      expect($('.player').find('nav')).toExist();
    });
    
    it("should have 4 buttons", function(){
      expect($('.player').find('button').length).toEqual(4);
      expect($('.player').find('button').length).toNotEqual(5);
    });
    
    it("should have a play button", function(){
      expect($('.player').find('button').first()).toHaveClass('control play');
      expect($('.player').find('button').first()).toHaveText('Play');
    });
    
    it("should have a hidden pause button", function(){
      expect($('.player').find('button').eq(1)).toHaveClass('control pause');
      expect($('.player').find('button').eq(1)).toHaveAttr('style');
      expect($('.player').find('button').eq(1)).toBeHidden();
    });
    
    it("on initialization it should create this.audio property", function(){
      expect(this.view.audio.nodeName).toEqual("AUDIO");
    });
    
    
  });
  
  describe("custom methods", function(){
    beforeEach(function(){
      this.playerStub = sinon.stub(this.player);
    });
    
    it("should update track and have pause button show on updateState function", function(){
      this.spyUpdateTrack = sinon.spy(this.view, "updateTrack");
      this.view.updateState();
      expect($('.player').find('button.play')).toBeHidden();
      expect(this.spyUpdateTrack).toHaveBeenCalledOnce();
      this.spyUpdateTrack.restore();
    });

    
    it("updateTrack should set audio object source and play or pause it", function(){
      this.audioStub = sinon.stub(this.view.audio);
      this.view.updateTrack();
      expect(this.playerStub.currentTrackUrl).toHaveBeenCalledWith();
      expect(this.audioStub.pause).toHaveBeenCalledWith();
    });
  });
  
  describe("events", function(){
    beforeEach(function(){
      this.playerStub = sinon.stub(this.player);
    });
    
    it("should call player.play when play is clicked", function(){
      $('.player').find('.play').trigger('click');
      expect(this.playerStub.play).toHaveBeenCalled();
    });
    
    it("should call player.pause when pause is clicked", function(){
      $('.player').find('.pause').trigger('click');
      expect(this.playerStub.pause).toHaveBeenCalled();
    });
    
    it("should call player.nextTrack when next is clicked", function(){
      $('.player').find('.next').trigger('click');
      expect(this.playerStub.nextTrack).toHaveBeenCalled();
    });
    
    it("should call player.prevTrack when prev is clicked", function(){
      $('.player').find('.prev').trigger('click');
      expect(this.playerStub.prevTrack).toHaveBeenCalled();
    });
    
  });
  
  
});

