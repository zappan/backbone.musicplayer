describe("Router", function(){
  describe("Routes", function(){
    
    beforeEach(function(){
      this.router = new MusicPlayer.Router();
      this.routeSpy = sinon.spy();
      try{
        Backbone.history.start();
      } catch(e) {}
      this.router.navigate("elsewhere");

      this.router.bind("route:home", this.routeSpy);
      this.libViewSpy     = sinon.spy(libraryView,  "render");
      this.plylistViewSpy = sinon.spy(playlistView, "render");
      this.plyrViewSpy    = sinon.spy(playerView,   "render");
      this.router.navigate("", true);  // backbone poziva matching route metode i triggerira route evente
    });
    
    it("fires the index route with a blank hash", function(){
      expect(this.routeSpy).toHaveBeenCalledOnce();
      expect(this.routeSpy).toHaveBeenCalledWith();
    });
    
    it("should call render methods on libraryView, playerView, playlistView", function(){
      expect(this.libViewSpy).toHaveBeenCalledOnce();
      expect(this.plylistViewSpy).toHaveBeenCalledOnce();
      expect(this.plyrViewSpy).toHaveBeenCalledOnce();      
    });
    
    
    describe("Router initialize method", function(){
      beforeEach(function(){
        this.librarySpy      = sinon.spy(MusicPlayer, "Library");
        this.libraryViewSpy  = sinon.spy(MusicPlayer, "LibraryView");
        this.playerSpy       = sinon.spy(MusicPlayer, "Player");
        this.playerViewSpy   = sinon.spy(MusicPlayer, "PlayerView");
        this.playlistSpy     = sinon.spy(MusicPlayer, "Playlist");
        this.playlistViewSpy = sinon.spy(MusicPlayer, "PlaylistView");
      });
      
      afterEach(function(){
        MusicPlayer.Library.restore();
      });
      
      it("should call correct classes in initialize method", function(){
        this.routerForInitialize = new MusicPlayer.Router();
        expect(this.librarySpy).toHaveBeenCalledOnce();
        expect(this.libraryViewSpy).toHaveBeenCalledOnce();
        expect(this.playerSpy).toHaveBeenCalledOnce();
        expect(this.playerViewSpy).toHaveBeenCalledOnce();
        expect(this.playlistSpy).toHaveBeenCalledOnce();
        expect(this.playlistViewSpy).toHaveBeenCalledOnce();
      });
      
    });
  });
});
